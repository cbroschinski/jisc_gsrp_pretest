



# Introduction

This is a small preliminary study into the research plan on "Global south lead authors" proposed by Jisc. Since the planned analysis will depend on large amounts of article metadata we currently do not have access to (Web of Science), it seemed adequate to create some of that data in advance using alternative methods and then try to process it. This would hopefully give us some insights into possible difficulties and challenges and also give us a clearer picture on the required time frame for the project. All data files, scripts and images are provided in the [Git repository](..). 

# Methodology

The largest transformative agreement to be analysed in the GSLA project will probably be the Springer Compact deal. OpenAPC has worked with metadata from several Springer Compact agreements from different countries for a long time and during our work on ["Springer Compact Coverage"](https://www.ub.uni-bielefeld.de/~cbroschinski/presentations/slidy/2019_01_17_Workshop_for_OA_data_experts.xhtml) we also obtained experience with Springers hosting platform SpringerLink and how to access their contents using web scraping methods. 

## Data scraping

Building upon our knowledge, we created a [programming script](fetch_springer_author_affiliations.py) which executes the following workflow:

1. Take an [official list of Springer journals](2018.csv) for a certain year (here: 2018) and filter it for those eligible for the Springer Compact programme (this resulted in  1939 out of 3105 journals).

2. For every journal, visit the journal search page on SpringerLink and search for all articles published in 2018 ([Example](https://link.springer.com/search?date-facet-mode=in&facet-start-year=2018&facet-end-year=2018&query=&facet-journal-id=125&facet-content-type=%22Article%22)).

3. From the resulting list, take the first 5 articles as a sample and extract the DOIs.

4. Navigate to the landing page for each article and open the "authors and affiliations" tab ([Example](https://link.springer.com/article/10.1007/s00125-018-4738-4#authorsandaffiliations)) (figuratively spoken, of course - since it's a scripted bot doing this, it can simply read the HTML source code and doesn't have to "click" anything)

5. Determine the lead author. Unfortunately it is not entirely clear what Springer considers to be the LA, but since there's usually one person with a mail icon and a linked address, we assumed this to be "corresponding author", who is hopefully identical with the lead author in the sense of the research plan (to our best knowledge, the Web of Science data is also based on the notion of a corresponding author (CA), so it's likely we're on the right track here).

6. Analyse the affiliations. As we can see, there's no direct textual link between authors and affiliations, instead they are mapped via index numbers. While this seems strange at first, it actually makes sense as it takes into account that an author might be affiliated with more than one institution (which happens quite often). It's also possible (albeit more rare) that there's more than one corresponding author for the same article. When mapping affiliations to corresponding authors, we handled possible conflicts by the following policy (keep in mind that we are eventually interested in countries, not institutions):
    - If a CA has more than one affiliation, go ahead if they are all located in the same country. Otherwise skip the article (8).
    - If there's more than one CA, go ahead if their affiliations are all located the same country. Otherwise skip the article (8).

While the process of matching affiliations to authors turned out to be tricky, the final step of determinig the countries was surprisingly easy. It's not visible in the web front end, but the affiliation strings are structured with HTML tags, so the information could be extracted without further effort. Every affiliation consists of up to four elements: Department, institution, city and country. While the first two are optional in some cases, the city and country information always seem to be present. 
    
7. Analyse the article type. This information can found in the "authors" tab above the "First online" date. Unfortunately this field doesn't seem to be normalised, so we may encounter whatever description the journal editor deemed appropriate. We used a blacklist to skip some cases which probably should not be treated as journal articles (like "correction" or "PhD thesis").

8. Whenever a journal is skipped during his workflow, go back to step 3 and process the next DOI in the list. Repeat until either 5 articles have been sucessfully analysed for each journal or the list is exhausted. 

The result of this workflow is a [JSON file](results.json). Since the obtained data on CAs is still multi-dimensional (possibly multiple corresponding authors, multiple affiliations), we would now "flatten" it by picking one author and one of their affiliations at random and treat it as "the author" for the given article (Since we have established during the aggregation that all CA and affiliations are based in the same country, we can do so without losing any relevant data precision). While doing this, the data is also transformed into a [CSV file](results.csv) which is easier to process. We ended up with with a total sample of 9,383 articles.

## Adding World Bank data

Having created a table of articles with author affiliations and countries, our next step was to add a classification scheme regarding "Global South" countries. According to [Wikipedia](https://en.wikipedia.org/w/index.php?title=Global_South&oldid=921971831), the term "Global South" was initially coined by the World Bank to refer to "low and middle income countries located in Asia, Africa, Latin America and the Caribbean". The article also points out that the term seems to be rather vague and an authoritative definition on what countries form the "Global South" is missing (it's even unclear if the term is more a political, an economical or a geographical concept). Since this is a political question which is clearly outside our field of work, we decided to stick to the approach proposed by the research plan (which is also more data-driven): In their [data repository](https://datahelpdesk.worldbank.org/knowledgebase/articles/906519-world-bank-country-and-lending-groups) the World Bank provides a [list of countries](http://databank.worldbank.org/data/download/site-content/CLASS.xls), classified into four groups regarding to income: Low, lower middle, upper middle, and high income. 

Working with the country as mapping key, we were able to use this table to enrich our own list of articles. We added the income group and the region to each article, and also a boolean identifier indicating if the country is considered to be part of the Global South. Again sticking to the World Bank definition, we classified countries with high income as FALSE (not part of the GS) and everything else as TRUE.

A specific problem we encountered when performing the mapping was heterogenity in country names. As it turns out, country designations are not normalised across journals in the Springer portfolio, so we had to build a normalisation table. The two Chinas and South Korea proved to be especially problematic, but there's also a problem with European countries being named in local languages (for example "Italia" or "Italien" instead of "Italy"). The final normalisation table can be found [here](fetch_springer_author_affiliations.py#L31).

# Results

This table shows a full overview of the aggregated data, grouped by the correspondings author's country.


|Country                |Region                     |Income              |Global South | Articles|
|:----------------------|:--------------------------|:-------------------|:------------|--------:|
|United States          |North America              |High income         |FALSE        |     1839|
|China                  |East Asia & Pacific        |Upper middle income |TRUE         |     1085|
|Germany                |Europe & Central Asia      |High income         |FALSE        |      937|
|India                  |South Asia                 |Lower middle income |TRUE         |      600|
|Japan                  |East Asia & Pacific        |High income         |FALSE        |      401|
|United Kingdom         |Europe & Central Asia      |High income         |FALSE        |      393|
|Italy                  |Europe & Central Asia      |High income         |FALSE        |      364|
|Canada                 |North America              |High income         |FALSE        |      275|
|Iran, Islamic Rep.     |Middle East & North Africa |Upper middle income |TRUE         |      246|
|France                 |Europe & Central Asia      |High income         |FALSE        |      215|
|Australia              |East Asia & Pacific        |High income         |FALSE        |      211|
|Brazil                 |Latin America & Caribbean  |Upper middle income |TRUE         |      198|
|Korea, Rep.            |East Asia & Pacific        |High income         |FALSE        |      191|
|Spain                  |Europe & Central Asia      |High income         |FALSE        |      172|
|Austria                |Europe & Central Asia      |High income         |FALSE        |      161|
|Netherlands            |Europe & Central Asia      |High income         |FALSE        |      152|
|Turkey                 |Europe & Central Asia      |Upper middle income |TRUE         |      128|
|Switzerland            |Europe & Central Asia      |High income         |FALSE        |      115|
|Poland                 |Europe & Central Asia      |High income         |FALSE        |      110|
|Sweden                 |Europe & Central Asia      |High income         |FALSE        |      102|
|Russian Federation     |Europe & Central Asia      |Upper middle income |TRUE         |       82|
|Belgium                |Europe & Central Asia      |High income         |FALSE        |       76|
|Mexico                 |Latin America & Caribbean  |Upper middle income |TRUE         |       76|
|Taiwan, China          |East Asia & Pacific        |High income         |FALSE        |       62|
|Israel                 |Middle East & North Africa |High income         |FALSE        |       61|
|Denmark                |Europe & Central Asia      |High income         |FALSE        |       58|
|Finland                |Europe & Central Asia      |High income         |FALSE        |       53|
|Greece                 |Europe & Central Asia      |High income         |FALSE        |       49|
|Czech Republic         |Europe & Central Asia      |High income         |FALSE        |       48|
|Norway                 |Europe & Central Asia      |High income         |FALSE        |       47|
|Singapore              |East Asia & Pacific        |High income         |FALSE        |       44|
|South Africa           |Sub-Saharan Africa         |Upper middle income |TRUE         |       44|
|Egypt, Arab Rep.       |Middle East & North Africa |Lower middle income |TRUE         |       42|
|Ireland                |Europe & Central Asia      |High income         |FALSE        |       42|
|Portugal               |Europe & Central Asia      |High income         |FALSE        |       42|
|New Zealand            |East Asia & Pacific        |High income         |FALSE        |       41|
|Malaysia               |East Asia & Pacific        |Upper middle income |TRUE         |       39|
|Argentina              |Latin America & Caribbean  |Upper middle income |TRUE         |       38|
|Pakistan               |South Asia                 |Lower middle income |TRUE         |       35|
|Hong Kong SAR, China   |East Asia & Pacific        |High income         |FALSE        |       31|
|Chile                  |Latin America & Caribbean  |High income         |FALSE        |       28|
|Nigeria                |Sub-Saharan Africa         |Lower middle income |TRUE         |       28|
|Thailand               |East Asia & Pacific        |Upper middle income |TRUE         |       28|
|Saudi Arabia           |Middle East & North Africa |High income         |FALSE        |       25|
|Tunisia                |Middle East & North Africa |Lower middle income |TRUE         |       25|
|Hungary                |Europe & Central Asia      |High income         |FALSE        |       23|
|Vietnam                |East Asia & Pacific        |Lower middle income |TRUE         |       22|
|Romania                |Europe & Central Asia      |Upper middle income |TRUE         |       21|
|Algeria                |Middle East & North Africa |Upper middle income |TRUE         |       18|
|Serbia                 |Europe & Central Asia      |Upper middle income |TRUE         |       14|
|Slovak Republic        |Europe & Central Asia      |High income         |FALSE        |       14|
|Morocco                |Middle East & North Africa |Lower middle income |TRUE         |       13|
|Slovenia               |Europe & Central Asia      |High income         |FALSE        |       13|
|Ukraine                |Europe & Central Asia      |Lower middle income |TRUE         |       11|
|Bangladesh             |South Asia                 |Lower middle income |TRUE         |        9|
|Colombia               |Latin America & Caribbean  |Upper middle income |TRUE         |        8|
|Croatia                |Europe & Central Asia      |High income         |FALSE        |        8|
|Estonia                |Europe & Central Asia      |High income         |FALSE        |        8|
|Jordan                 |Middle East & North Africa |Upper middle income |TRUE         |        8|
|Ethiopia               |Sub-Saharan Africa         |Low income          |TRUE         |        7|
|Lebanon                |Middle East & North Africa |Upper middle income |TRUE         |        7|
|Uruguay                |Latin America & Caribbean  |High income         |FALSE        |        7|
|Bulgaria               |Europe & Central Asia      |Upper middle income |TRUE         |        6|
|Ghana                  |Sub-Saharan Africa         |Lower middle income |TRUE         |        6|
|Indonesia              |East Asia & Pacific        |Lower middle income |TRUE         |        6|
|Kuwait                 |Middle East & North Africa |High income         |FALSE        |        6|
|Lithuania              |Europe & Central Asia      |High income         |FALSE        |        6|
|Luxembourg             |Europe & Central Asia      |High income         |FALSE        |        6|
|Qatar                  |Middle East & North Africa |High income         |FALSE        |        6|
|United Arab Emirates   |Middle East & North Africa |High income         |FALSE        |        6|
|Cyprus                 |Europe & Central Asia      |High income         |FALSE        |        5|
|Kenya                  |Sub-Saharan Africa         |Lower middle income |TRUE         |        5|
|Oman                   |Middle East & North Africa |High income         |FALSE        |        5|
|Philippines            |East Asia & Pacific        |Lower middle income |TRUE         |        5|
|Bosnia and Herzegovina |Europe & Central Asia      |Upper middle income |TRUE         |        4|
|Iceland                |Europe & Central Asia      |High income         |FALSE        |        4|
|Tanzania               |Sub-Saharan Africa         |Low income          |TRUE         |        4|
|Cuba                   |Latin America & Caribbean  |Upper middle income |TRUE         |        3|
|Ecuador                |Latin America & Caribbean  |Upper middle income |TRUE         |        3|
|Iraq                   |Middle East & North Africa |Upper middle income |TRUE         |        3|
|Kazakhstan             |Europe & Central Asia      |Upper middle income |TRUE         |        3|
|Malta                  |Middle East & North Africa |High income         |FALSE        |        3|
|Uganda                 |Sub-Saharan Africa         |Low income          |TRUE         |        3|
|West Bank and Gaza     |Middle East & North Africa |Lower middle income |TRUE         |        3|
|Armenia                |Europe & Central Asia      |Upper middle income |TRUE         |        2|
|Belarus                |Europe & Central Asia      |Upper middle income |TRUE         |        2|
|Benin                  |Sub-Saharan Africa         |Low income          |TRUE         |        2|
|Botswana               |Sub-Saharan Africa         |Upper middle income |TRUE         |        2|
|Cameroon               |Sub-Saharan Africa         |Lower middle income |TRUE         |        2|
|Costa Rica             |Latin America & Caribbean  |Upper middle income |TRUE         |        2|
|Montenegro             |Europe & Central Asia      |Upper middle income |TRUE         |        2|
|North Macedonia        |Europe & Central Asia      |Upper middle income |TRUE         |        2|
|Burkina Faso           |Sub-Saharan Africa         |Low income          |TRUE         |        1|
|Côte d'Ivoire          |Sub-Saharan Africa         |Lower middle income |TRUE         |        1|
|Fiji                   |East Asia & Pacific        |Upper middle income |TRUE         |        1|
|Georgia                |Europe & Central Asia      |Upper middle income |TRUE         |        1|
|Greenland              |Europe & Central Asia      |High income         |FALSE        |        1|
|Jamaica                |Latin America & Caribbean  |Upper middle income |TRUE         |        1|
|Kyrgyz Republic        |Europe & Central Asia      |Lower middle income |TRUE         |        1|
|Liechtenstein          |Europe & Central Asia      |High income         |FALSE        |        1|
|Madagascar             |Sub-Saharan Africa         |Low income          |TRUE         |        1|
|Maldives               |South Asia                 |Upper middle income |TRUE         |        1|
|Nepal                  |South Asia                 |Low income          |TRUE         |        1|
|Peru                   |Latin America & Caribbean  |Upper middle income |TRUE         |        1|
|Syrian Arab Republic   |Middle East & North Africa |Low income          |TRUE         |        1|
|Togo                   |Sub-Saharan Africa         |Low income          |TRUE         |        1|
|Yemen, Rep.            |Middle East & North Africa |Low income          |TRUE         |        1|
|Zimbabwe               |Sub-Saharan Africa         |Lower middle income |TRUE         |        1|

We may also inspect the total share of articles from the Global South as a stacked bar chart showing the regional composition:

![](figure/gs_large_by_regions.png)

As we can see, our initial classification of what constitutes the Global South is probably too broad (The share of Global South articles would be about 31%). The problem is that it also includes the so-called BRICS states (Brazil, Russia, India, China, South Africa), all emerging economies very likely not to be considered as Global South in the context of the Jisc research plan - especially China, which is home to 1085 corresponding authors in our sample (12%).

If we move all countries which are "Upper middle income" economies into the Global North category, the distribution changes considerably. Here's a table showing only the articles from the Global South under this definition:


|Country              |Region                     |Income              |Global South | Articles|
|:--------------------|:--------------------------|:-------------------|:------------|--------:|
|India                |South Asia                 |Lower middle income |TRUE         |      600|
|Egypt, Arab Rep.     |Middle East & North Africa |Lower middle income |TRUE         |       42|
|Pakistan             |South Asia                 |Lower middle income |TRUE         |       35|
|Nigeria              |Sub-Saharan Africa         |Lower middle income |TRUE         |       28|
|Tunisia              |Middle East & North Africa |Lower middle income |TRUE         |       25|
|Vietnam              |East Asia & Pacific        |Lower middle income |TRUE         |       22|
|Morocco              |Middle East & North Africa |Lower middle income |TRUE         |       13|
|Ukraine              |Europe & Central Asia      |Lower middle income |TRUE         |       11|
|Bangladesh           |South Asia                 |Lower middle income |TRUE         |        9|
|Ethiopia             |Sub-Saharan Africa         |Low income          |TRUE         |        7|
|Ghana                |Sub-Saharan Africa         |Lower middle income |TRUE         |        6|
|Indonesia            |East Asia & Pacific        |Lower middle income |TRUE         |        6|
|Kenya                |Sub-Saharan Africa         |Lower middle income |TRUE         |        5|
|Philippines          |East Asia & Pacific        |Lower middle income |TRUE         |        5|
|Tanzania             |Sub-Saharan Africa         |Low income          |TRUE         |        4|
|Uganda               |Sub-Saharan Africa         |Low income          |TRUE         |        3|
|West Bank and Gaza   |Middle East & North Africa |Lower middle income |TRUE         |        3|
|Benin                |Sub-Saharan Africa         |Low income          |TRUE         |        2|
|Cameroon             |Sub-Saharan Africa         |Lower middle income |TRUE         |        2|
|Burkina Faso         |Sub-Saharan Africa         |Low income          |TRUE         |        1|
|Côte d'Ivoire        |Sub-Saharan Africa         |Lower middle income |TRUE         |        1|
|Kyrgyz Republic      |Europe & Central Asia      |Lower middle income |TRUE         |        1|
|Madagascar           |Sub-Saharan Africa         |Low income          |TRUE         |        1|
|Nepal                |South Asia                 |Low income          |TRUE         |        1|
|Syrian Arab Republic |Middle East & North Africa |Low income          |TRUE         |        1|
|Togo                 |Sub-Saharan Africa         |Low income          |TRUE         |        1|
|Yemen, Rep.          |Middle East & North Africa |Low income          |TRUE         |        1|
|Zimbabwe             |Sub-Saharan Africa         |Lower middle income |TRUE         |        1|

Under this circumstances the share of articles from the Global South goes down to 8.9%, with India being the biggest player left:

![](figure/gs_med_by_regions.png)

Finally, if we define the Global South as only those countries with a low income, the group gets very small with mostly authors affiliated with institutions in Sub-Saharan Africa:


|Country              |Region                     |Income     |Global South | Articles|
|:--------------------|:--------------------------|:----------|:------------|--------:|
|Ethiopia             |Sub-Saharan Africa         |Low income |TRUE         |        7|
|Tanzania             |Sub-Saharan Africa         |Low income |TRUE         |        4|
|Uganda               |Sub-Saharan Africa         |Low income |TRUE         |        3|
|Benin                |Sub-Saharan Africa         |Low income |TRUE         |        2|
|Burkina Faso         |Sub-Saharan Africa         |Low income |TRUE         |        1|
|Madagascar           |Sub-Saharan Africa         |Low income |TRUE         |        1|
|Nepal                |South Asia                 |Low income |TRUE         |        1|
|Syrian Arab Republic |Middle East & North Africa |Low income |TRUE         |        1|
|Togo                 |Sub-Saharan Africa         |Low income |TRUE         |        1|
|Yemen, Rep.          |Middle East & North Africa |Low income |TRUE         |        1|

Now the share of articles from the Global South is only 0.23%.

![](figure/gs_small_by_regions.png)

# Conclusions

- We have shown that the approach suggested by the Jisc Research plan is viable. We didn't follow it through completely (The part on calculating effective APCs was left out), but that shouldn't pose a significant problem.
- The crucial point will be access to Web of Science data. While the web scraping approach used in this study yielded enough data to perform a preliminary analysis, it does not scale well with the larger amounts required by the research plan. As we know from our work in the Springer Compact Coverage project, the total number of articles published in journals eligibe for Springer Compact UK in 2018 was over 212,000, so our work only obtained about 4.4% of those. Datamining the rest of those articles in the same way would take much too long and is potentially unwanted by Springer as it puts a heavy load on their web site.
- Much will depend on how and in what format affiliations are provided by the Web of Science. As we saw in our approach, determining the author's country on SpringerLink is simple as it is directly marked up in the HTML source code - this might be not so easy with the WoS data.
- A dependable definition of what countries should constitute the "Global South" in terms of the Research Plan is needed. As we saw, using the "standard" definition by the World Bank would span a large share of the countries in the world, including big players like Russia, China, India or Brazil. 

