#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import csv
import json
import os
import re
import sys

import openapc_toolkit as oat

from time import sleep
from urllib.request import urlopen, Request

SPRINGER_JOURNAL_ARTICLES_URL = "https://link.springer.com/search?facet-content-type=%22Article%22&sortOrder=newestFirst&facet-journal-id={}&date-facet-mode=in&facet-start-year=2018&facet-end-year=2018"
SPRINGER_JOURNAL_AUTHORS_URL = "https://link.springer.com/article/{}#authorsandaffiliations"
# Will only capture at most 3 affiliations
SPRINGER_ARTICLE_AFFILIATIONS = re.compile('<li class="affiliation" data-test="affiliation-(?P<affiliation_number>\d+)"[^<>]*><span class="affiliation__count">[^<>]*</span><span class="affiliation__item">(?:<span itemprop="department" class="affiliation__department">(?P<affiliation_department>[^<>]*)</span>)?(?:<span itemprop="name" class="affiliation__name">(?P<affiliation_name>[^<>]*)</span>)?<span itemprop="address"[^<>]*><span itemprop="addressRegion" class="affiliation__city">(?P<affiliation_city>[^<>]*)</span><span itemprop="addressCountry" class="affiliation__country">(?P<affiliation_country>[^<>]*)</span>')

SPRINGER_ARTICLE_AUTHORS_AFFI_NUMBERS = re.compile('class="authors-affiliations__name">([^<>]+)</span><ul class="authors-affiliations__indexes u-inline-list" data-role="AuthorsIndexes">(?:<li data-affiliation=[^<>]+>([^<>]+)</li>)(?:<li data-affiliation=[^<>]+>([^<>]+)</li>)?(?:<li data-affiliation=[^<>]+>([^<>]+)</li>)?</ul><span class="author-information"><span class="author-information__contact u-icon-before">')
DOI_SEARCH_STRING = re.compile(' doi="(?P<doi>.*?)" ', re.IGNORECASE)
ARTICLE_TYPE = re.compile('<span class="test-render-category">(?P<article_type>.*?)</span>')

OC_JOURNALS = {}
BATCH_SIZE = 200
MAX_DOIS_PER_JOURNAL = 5
RESULTS_FILE = "results.json"

TYPES_BL = ["PhD Thesis", "Editorial", "Correction"]

COUNTRY_NORM = {
    "Iran": "Iran, Islamic Rep.",
    "Islamic Republic of Iran": "Iran, Islamic Rep.",
    "USA": "United States",
    "U.S.A.": "United States",
    "South Korea": "Korea, Rep.",
    "Korea": "Korea, Rep.",
    "Republic of Korea": "Korea, Rep.",
    "Korea (Republic of)": "Korea, Rep.",
    "People’s Republic of China": "China",
    "Volksrepublik China": "China",
    "P.R. China": "China",
    "P. R. China": "China",
    "PR China": "China",
    "Hong Kong": "Hong Kong SAR, China",
    "Hong Kong SAR": "Hong Kong SAR, China",
    "Hong Kong, China": "Hong Kong SAR, China",
    "Deutschland": "Germany",
    "UK": "United Kingdom",
    "The Netherlands": "Netherlands",
    "the Netherlands": "Netherlands",
    "Slovakia": "Slovak Republic",
    "Russia": "Russian Federation",
    "Republic of China": "Taiwan, China",
    "Taiwan (R.O.C.)": "Taiwan, China",
    "Taiwan, ROC": "Taiwan, China",
    "Taiwan": "Taiwan, China",
    "Taiwan, Republic of China": "Taiwan, China",
    "Scotland, UK": "United Kingdom",
    "England, UK": "United Kingdom",
    "England": "United Kingdom",
    "Kingdom of Saudi Arabia": "Saudi Arabia",
    "Egypt": "Egypt, Arab Rep.",
    "Bénin": "Benin",
    "Republic of Benin": "Benin",
    "Republic of Singapore": "Singapore",
    "Schweiz": "Switzerland",
    "Svizzera": "Switzerland",
    "Österreich": "Austria",
    "México": "Mexico",
    "Republic of Belarus": "Belarus",
    "Côte d’Ivoire": "Côte d'Ivoire",
    "Grand Duchy of Luxembourg": "Luxembourg",
    "Yemen": "Yemen, Rep.",
    "Finnland": "Finland",
    "Italia": "Italy",
    "Italien": "Italy",
    "Palestine": "West Bank and Gaza",
    "Republic of Macedonia": "North Macedonia",
    "Macedonia": "North Macedonia",
    "Schweden": "Sweden",
    "Republic of South Africa": "South Africa",
    "UAE": "United Arab Emirates",
    "Spanien": "Spain",
    "España": "Spain",
    "Tunisie": "Tunisia",
    "Syria": "Syrian Arab Republic",
    "Brasil": "Brazil",
    "Ungarn": "Hungary",
    "Republic of Ireland": "Ireland",
    "State of Kuwait": "Kuwait",
    "North Cyprus": "Cyprus",
    "Kyrgyzstan": "Kyrgyz Republic",
    "Great Britain": "United Kingdom",
    "Belgien": "Belgium",
    "Czechia": "Czech Republic"
}

def fetch_article_dois(journal_id):
    url = SPRINGER_JOURNAL_ARTICLES_URL.format(journal_id)
    req = Request(url, None)
    response = urlopen(req)
    content = response.read()
    content = content.decode("utf-8")
    return DOI_SEARCH_STRING.findall(content)
    
def fetch_article_content(doi):
    url = SPRINGER_JOURNAL_AUTHORS_URL.format(doi)
    req = Request(url, None)
    response = urlopen(req)
    content = response.read()
    content = content.decode("utf-8")
    return content
    
def obtain_artice_type(doi, content):
    article_type = ARTICLE_TYPE.search(content).groupdict()["article_type"]
    if article_type in TYPES_BL:
        msg = "Error: Article {} is of blacklisted type {}!"
        raise ValueError(msg.format(doi, article_type))
    return article_type
    
def obtain_ca_and_affiliation(content, journal_id, doi):
    msg = "Obtaining data for doi {} in journal {}..."
    oat.print_g(msg.format(doi, journal_id))
    authors = SPRINGER_ARTICLE_AUTHORS_AFFI_NUMBERS.findall(content)
    if not authors:
        msg = "Error: Could not obtain authors for article {}!"
        raise ValueError(msg.format(doi))
    affiliations = SPRINGER_ARTICLE_AFFILIATIONS.findall(content)
    if not affiliations:
        msg = "Error: Could not obtain affiliations for article {}!"
        raise ValueError(msg.format(doi))
    affi_map = {}
    for affi in affiliations:
        affi_map[affi[0]] = {
            "department": affi[1],
            "institution": affi[2],
            "city": affi[3],
            "country": affi[4]
        }
    result = {}
    for group in authors:
        author_countries = []
        name = group[0].replace("\u00a0", " ")
        result[name] = []
        for item in group[1:4]:
            if item != "":
                result[name].append(affi_map[item])
                country = affi_map[item]["country"]
                if country in COUNTRY_NORM:
                    msg = 'Normalisation: Replaced country name "{}" by normalised World Bank designation "{}"'
                    oat.print_y(msg.format(country, COUNTRY_NORM[country]))
                    result[name][-1]["country"] = COUNTRY_NORM[country]
                if country not in author_countries:
                    author_countries.append(country)
        if len(author_countries) > 1:
            msg = 'Error: CA "{}" of article {} has affiliations within more than one country ({})!'
            raise ValueError(msg.format(name, doi, ", ".join(author_countries)))
    # We have established that each individual CA has affis in the same country by now, so we can now check for inter-author conflicts 
    countries = []
    for  author, affiliations in result.items():
        if affiliations[0]["country"] not in countries:
            countries.append(country)
    if len(countries) > 1:
            msg = 'Error: Article {} has CAs with affiliations in different countries ({})!'
            raise ValueError(msg.format(doi, ", ".join(countries)))
    return result
    
if os.path.isfile(RESULTS_FILE):
    with open(RESULTS_FILE, "r") as f:
        try:
            OC_JOURNALS  = json.loads(f.read())
            print("results cache file sucessfully loaded.")
        except ValueError:
            print("Could not decode a cache structure from " + RESULTS_FILE + "!")
            sys.exit()

reader = csv.DictReader(open("2018.csv", "r"))
count = 0
for line in reader:
    oa_option = line["OpenAccessOption"]
    journal_id = line["product_id"]
    title = line["Title"]
    if oa_option != "Hybrid (Open Choice)":
        msg = 'Journal {} ("{}") is no Open Choice journal, skipping...'
        oat.print_r(msg.format(journal_id, title))
        continue
    if journal_id not in OC_JOURNALS:
        OC_JOURNALS[journal_id] = {
            "title": title,
            "articles": {}
        }
        msg = 'Fetching DOIs for journal {} ("{}")'
        oat.print_b(msg.format(journal_id, title))
        doi_matches = fetch_article_dois(journal_id)
        sleep(1)
        for doi_string in doi_matches:
            norm_doi = oat.get_normalised_DOI(doi_string)
            if norm_doi is not None and norm_doi not in OC_JOURNALS[journal_id]["articles"]:
                content = fetch_article_content(norm_doi)
                sleep(1)
                count += 1
                try:
                    article_type = obtain_artice_type(norm_doi, content)
                    authors = obtain_ca_and_affiliation(content, journal_id, norm_doi)
                except ValueError as ve:
                    oat.print_r(str(ve))
                    continue
                OC_JOURNALS[journal_id]["articles"][norm_doi] = {}
                OC_JOURNALS[journal_id]["articles"][norm_doi]["corresponding_authors"] = authors
                OC_JOURNALS[journal_id]["articles"][norm_doi]["type"] = article_type
                if len(OC_JOURNALS[journal_id]["articles"]) == MAX_DOIS_PER_JOURNAL:
                    break
        if count >= BATCH_SIZE:
            break
    else:
        msg = 'Journal {} ("{}") already cached, skipping...'
        oat.print_y(msg.format(journal_id, title))
        
with open(RESULTS_FILE, "w", encoding="utf-8") as f:
    f.write(json.dumps(OC_JOURNALS, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': ')))
    f.flush()
    
