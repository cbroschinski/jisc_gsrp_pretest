#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import csv
import json

with open("results.json") as json_file:
    content = json.loads(json_file.read())

countries_map = {}
reader = csv.DictReader(open("worldbank_countries.csv", "r"))
for line in reader:
    country = line["Economy"]
    region = line["Region"]
    group = line["Income group"]
    global_south = "TRUE"
    if group == "High income":
        global_south = "FALSE"
    countries_map[country] = {
        "region": region,
        "group": group,
        "global_south": global_south
    }

    
csv_header = ["journal_id", "journal_title", "doi", "article_type", "corresponding_author", "department", "institution", "city", "country", "region", "income_group", "global_south"]

lines = [csv_header]

for journal_id, journal in content.items():
    journal_title = journal["title"]
    for doi, doi_content in journal["articles"].items():
        article_type = doi_content["type"]
        new_line = [journal_id, journal_title, doi, article_type]
        for author, affi in doi_content["corresponding_authors"].items():
            country = affi[0]["country"]
            if country not in countries_map:
                msg = "Country " + country + " not found in World Bank country list!"
                raise ValueError(msg)
            new_line.append(author)
            new_line.append(affi[0]["department"])
            new_line.append(affi[0]["institution"])
            new_line.append(affi[0]["city"])
            new_line.append(country)
            new_line.append(countries_map[country]["region"])
            new_line.append(countries_map[country]["group"])
            new_line.append(countries_map[country]["global_south"])
            break
        lines.append(new_line)

with open("results.csv", "w") as out_file:
    writer = csv.writer(out_file)
    writer.writerows(lines) 
